package com.test.textreader;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.test.textreader.models.Authors;
import com.test.textreader.models.Books;
import com.test.textreader.models.Genres;

import java.util.List;

public class EditBookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_book);

        final Books book = Books.findById(Books.class, getIntent().getLongExtra("id", 0));

        EditText etBookName = (EditText) findViewById(R.id.etBookName);
        final EditText etAuthor = (EditText) findViewById(R.id.etAuthor);
        final EditText etGenre = (EditText) findViewById(R.id.etGenre);
        final EditText etDesc = (EditText) findViewById(R.id.etDesc);

        Button btnSave = (Button) findViewById(R.id.btnSave);

        etBookName.setText(book.getTitle());
        etDesc.setText(book.getDescription());

        if (book.getAuthor() != null) {
            etAuthor.setText(book.getAuthor().getName());
        }

        if (book.getGenre() != null) {
            etGenre.setText(book.getGenre().getGenreName());
        }

        assert btnSave != null;
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                book.setDescription(etDesc.getText().toString());
                String authorName = etAuthor.getText().toString();
                String genreName = etGenre.getText().toString();

                List<Authors> findedAuthors = Authors.find(Authors.class, "name = ?", authorName);
                List<Genres> findedGenres = Genres.find(Genres.class, "name = ?", genreName);

                Authors author;
                Genres genre;

                if (findedAuthors.size() == 0) {
                    author = new Authors(authorName);
                    author.save();
                } else {
                    author = findedAuthors.get(0);
                }

                book.setAuthor(author);

                if (findedGenres.size() == 0) {
                    genre = new Genres(genreName);
                    genre.save();
                } else {
                    genre = findedGenres.get(0);
                }

                book.setGenre(genre);

                book.save();

                finish();

            }
        });
    }
}
