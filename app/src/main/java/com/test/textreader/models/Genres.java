package com.test.textreader.models;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.io.Serializable;

/**
 * Created by andrey on 31.08.16.
 */
@Table
public class Genres extends SugarRecord implements Serializable {

    @Column(name = "name")
    private String genreName;

    public Genres() {
    }

    public Genres(String genreName) {
        this.genreName = genreName;
    }

    public String getGenreName() {
        return genreName;
    }
}
