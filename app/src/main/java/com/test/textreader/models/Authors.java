package com.test.textreader.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.io.Serializable;

/**
 * Created by andrey on 31.08.16.
 */
@Table
public class Authors extends SugarRecord implements Serializable {

    private int _id;
    private String name;

    public Authors() {
    }

    public Authors(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
