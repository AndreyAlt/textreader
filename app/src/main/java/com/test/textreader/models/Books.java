package com.test.textreader.models;

import android.os.Parcelable;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.io.Serializable;

/**
 * Created by andrey on 31.08.16.
 */
@Table
public class Books extends SugarRecord implements Serializable {

    private String title;
    private String description;
    private String text;

    private Long author_id;
    private Long genre_id;

    public Books() {
    }

    public Books(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public Genres getGenre() {
        if (genre_id != null) {
            return Genres.findById(Genres.class, genre_id);
        }
        return null;
    }

    public Authors getAuthor() {
        if (author_id != null) {
            return Authors.findById(Authors.class, author_id);
        }
        return null;
    }

    public void setAuthor(Authors author) {
        this.author_id = author.getId();
    }

    public void setGenre(Genres genre) {
        this.genre_id = genre.getId();
    }
}
