package com.test.textreader.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.test.textreader.R;
import com.test.textreader.extras.UiUtil;

/**
 * Created by andrey on 29.08.16.
 */
public class MyTextView extends TextView {

    public MyTextView(Context context) {
        super(context);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_autocop_textreader_views_MyTextView,
                R.styleable.com_autocop_textreader_views_MyTextView_font);
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_autocop_textreader_views_MyTextView,
                R.styleable.com_autocop_textreader_views_MyTextView_font);
    }

}
