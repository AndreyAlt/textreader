package com.test.textreader.adapters;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.textreader.EditBookActivity;
import com.test.textreader.PagerActivity;
import com.test.textreader.R;
import com.test.textreader.models.Books;

import java.io.Serializable;
import java.util.List;

/**
 * Created by andrey on 01.09.16.
 */
public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    List<Books> booksList;

    public BooksAdapter(List<Books> dataset) {
        booksList = dataset;
    }

    @Override
    public BooksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(BooksAdapter.ViewHolder holder, int position) {
        holder.setItem(booksList.get(position));
    }

    @Override
    public int getItemCount() {
        return booksList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private static final String TAG = "MY_LOG";

        private Books mItem;

        private TextView tvTitle;
        private TextView tvAuthor;
        private TextView tvGenre;
        private ImageView ivEditBook;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);

            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvAuthor = (TextView) v.findViewById(R.id.tvAuthor);
            tvGenre = (TextView) v.findViewById(R.id.tvGenre);
            ivEditBook = (ImageView) v.findViewById(R.id.ivEditBook);
        }

        public void setItem(Books item) {
            mItem = item;
            tvTitle.setText(mItem.getTitle());
            if (mItem.getAuthor() != null) {
                tvAuthor.setText(mItem.getAuthor().getName());
            } else {
                tvAuthor.setText("");
            }
            if (mItem.getGenre() != null) {
                tvGenre.setText(mItem.getGenre().getGenreName());
            } else {
                tvGenre.setText("");
            }

            ivEditBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), EditBookActivity.class);
                    intent.putExtra("id", mItem.getId());
                    view.getContext().startActivity(intent);

                }
            });

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), PagerActivity.class);
            intent.putExtra("text", mItem.getText());
            v.getContext().startActivity(intent);
        }

    }

}
