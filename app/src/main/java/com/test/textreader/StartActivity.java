package com.test.textreader;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.test.textreader.adapters.BooksAdapter;
import com.test.textreader.extras.FolderScanner;
import com.test.textreader.models.Books;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class StartActivity extends AppCompatActivity {

    List<String> booksList;

    RecyclerView rvBooks;
    BooksAdapter booksAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvBooks = (RecyclerView) findViewById(R.id.rvBooks);
        rvBooks.setLayoutManager(layoutManager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        booksList = new FolderScanner().getBooks(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));

        for (String path : booksList) {
            File file = new File(path);
            String fileName = file.getName();
            String text = null;

            List<Books> findedBooks = Books.find(Books.class, "title = ?", fileName);

            if (findedBooks.size() == 0) {
                try {
                    // открываем поток для чтения
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String str = "";
                    // читаем содержимое
                    while ((str = br.readLine()) != null) {
                        text += str;
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Books book = new Books(fileName, text);
                book.save();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        booksAdapter = new BooksAdapter(Books.listAll(Books.class));
        rvBooks.setAdapter(booksAdapter);
    }
}
