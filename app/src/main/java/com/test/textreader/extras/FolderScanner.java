package com.test.textreader.extras;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prg on 01.09.2016.
 */
public class FolderScanner {

    public List<String> getBooks(File path) {
        List<String> books = new ArrayList<>();
        if(path != null && path.canRead()) {
            for (File file : path.listFiles()) {
                if (file.isDirectory()) {
                    books.addAll(this.getBooks(file));
                } else {
                    if (file.getName().endsWith(".txt")) {
                        books.add(file.getAbsolutePath());
                    }
                }
            }
        }

        return books;
    }

}
