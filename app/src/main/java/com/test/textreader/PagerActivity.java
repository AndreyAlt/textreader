package com.test.textreader;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.WindowManager;
import android.widget.TextView;

import com.test.textreader.views.MyTextView;

import java.util.ArrayList;
import java.util.List;

public class PagerActivity extends AppCompatActivity {

    private static int height;
    private static int width;
    private static WindowManager windowManager;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    private MyTextView myTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        DisplayMetrics displaymetrics = new DisplayMetrics();
        windowManager = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;

        String text = getIntent().getStringExtra("text");

        myTextView = (MyTextView) findViewById(R.id.testTv);
        List<String> pages = getPages(text);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), pages);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        if (mViewPager != null) {
            mViewPager.setAdapter(mSectionsPagerAdapter);
        }

    }

    private List<String> getPages(String bookText) {
        List<String> pages = new ArrayList<String>();

        int spToPx = Math.round(14*(getResources().getDisplayMetrics().xdpi/DisplayMetrics.DENSITY_DEFAULT));
        int marginPx = Math.round(16*(getResources().getDisplayMetrics().xdpi/DisplayMetrics.DENSITY_DEFAULT));

//            37, 40
//            175

        int linesInPage = (int) (Math.floor((height - 175 - marginPx * 2) / spToPx));

        float[] measuredWidth = new float[1];
//        int cnt = myTextView.getPaint().breakText(getString(R.string.big_text), true, linesInPage/2 * width, measuredWidth);
//
//        int start = getString(R.string.big_text).length() < cnt * page - cnt ? 0 : cnt * page - cnt;
//        int end = getString(R.string.big_text).length() < cnt * page ? getString(R.string.big_text).length() : cnt * page;

        String[] strings = bookText.split("\n");
        String placedText = "";
        int freeSymbols = 0;

//        int symbolsInRow = myTextView.getPaint().breakText(getString(R.string.big_text), true, width-marginPx * 2, measuredWidth);
        int symbolsInRow = (int) (Math.floor((width - marginPx * 2)/14));
        freeSymbols = symbolsInRow * linesInPage;
        freeSymbols = myTextView.getPaint().breakText(bookText, true, (width-marginPx * 2) * linesInPage, measuredWidth);

        for (int i = 0; i < strings.length; i++) {
            if (freeSymbols > 0) {
                if (strings[i].length() != -1) {
                    if (strings[i].length() < symbolsInRow) {
                        placedText = placedText + strings[i] + "<br>";
                        freeSymbols = freeSymbols - symbolsInRow;
                    } else if (strings[i].length() < freeSymbols) {
                        placedText = placedText + strings[i] + "<br>";
                        freeSymbols = freeSymbols - (strings[i].length()/symbolsInRow*symbolsInRow+symbolsInRow);
                    } else {
                        placedText = placedText + strings[i].substring(0, freeSymbols) + "<br>";
                        freeSymbols = freeSymbols - strings[i].substring(0, freeSymbols).length();
                    }
                } else {
                    placedText = placedText + "\n";
                }
            }
            if (freeSymbols == 0) {
//                pages.add(placedText.replace(" ", "&nbsp;"));
                pages.add(placedText);
                placedText = "";
                freeSymbols = symbolsInRow * linesInPage;
            }
        }

        return pages;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_PAGE_TEXT = "page_text";


        int oldIndex = 0;

        String pageText;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber, String pageText) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putString(ARG_PAGE_TEXT, pageText);
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_pager, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            int page = getArguments().getInt(ARG_SECTION_NUMBER);
            String pageText = getArguments().getString(ARG_PAGE_TEXT);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//            textView.setText(pageText);



//            placedText.replace(" ", "&nbsp");
//            placedText.replace(" ", "U+00A0");


//            textView.setText(placedText);
            textView.setText(Html.fromHtml(pageText));

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        List<String> pages = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm, List<String> pages) {
            super(fm);
            this.pages = pages;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position, pages.get(position));
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return pages.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}
